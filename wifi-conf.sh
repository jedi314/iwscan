#!/bin/sh

# Configure the camera as an access point on wlan0
# and accept only a single client to connect


IF=wlan0
SRV_IP=1
CLI_IP_FIRST=100
CLI_IP_LAST=200

DHCP_CONF=/etc/dhcp/dhcpd.conf
DHCP_SUBNET=192.168.150.0
DHCP_NETMASK=255.255.255.0
DHCP_SRV_IP=$(echo $DHCP_SUBNET.$SRV_IP | awk -F. '{printf "%d.%d.%d.%d", $1, $2, $3, $5}')
DHCP_CLI_IP_FIRST=$(echo $DHCP_SUBNET.$CLI_IP_FIRST | awk -F. '{printf "%d.%d.%d.%d", $1, $2, $3, $5}')
DHCP_CLI_IP_LAST=$(echo $DHCP_SUBNET.$CLI_IP_LAST | awk -F. '{printf "%d.%d.%d.%d", $1, $2, $3, $5}')
DHCP_SCRIPT=S80dhcp-server

WPA_CONF=/etc/wpa_supplicant.conf
WPA_ESSID=BASIC2
WPA_PROTOCOL=WPA-PSK
WPA_PASS=piattaforma
WPA_BIN=wpa_supplicant
CONF_FILE=/etc/wifi.conf


print_help() {
	echo "Usage: configure as access point"
	echo "	setup_wlan.sh ap PARAMS"
	echo "Usage: configure as host"
	echo "	setup_wlan.sh host PROTOCOL PARAMS"
	echo "Usage: print help"
	echo "	setup_wlan.sh help"
	echo ""
	echo "Available PROTOCOL values: WPA-PSK, WPA-EAP"
	echo "PARAMS for WPA-PSK: ESSID PASSWORD"
	echo "PARAMS for WPA-EAP: ESSID IDENTITY"
	echo "PARAMS for ap: ESSID [CHANNEL]"
	echo ""
	echo "Examples"
	echo "Start the access point BASIC2:"
	echo "	setup_wlan.sh ap BASIC2"
	echo "Start the access point BASIC2 on channel 11:"
	echo "	setup_wlan.sh ap BASIC2 11"
	echo "Stop the access point:"
	echo "	setup_wlan.sh ap"
	echo "Connect to network HOME"
	echo "	setup_wlan.sh host WPA-PSK HOME"
	echo "Connect to network WORK with user marior"
	echo "	setup_wlan.sh host WPA-EAP WORK marior"
	echo "Disconnect from any network"
	echo "	setup_wlan.sh host"

	exit 0
}


start_ap() {
	echo "Starting Access Point on $IF"
	local ch=11
    local passphrase=$3

	[ -n "$2" ] && ch=$(printf "%02d" "$2")
	freq=$(iwlist $IF channel | grep "Channel $ch" | awk -F' ' '{printf "%.03f", $4}' | awk -F. '{printf "%d%d", $1, $2}')

	if test -z "$freq";then
		echo "Channel $ch not supported"
		exit 1
	fi

    
    cat > $WPA_CONF <<-_EOF_
	ctrl_interface=/var/run/wpa_supplicant
	ap_scan=1

	network={
	        ssid="$1"
	        mode=2
	        key_mgmt=$WPA_PROTOCOL
	        frequency=$freq
	_EOF_

    if [ -n "$passphrase" ]; then
        cat >> $WPA_CONF <<-_EOF_
        	psk="$passphrase"
	_EOF_
    fi
    cat >> $WPA_CONF <<-_EOF_
    }
	_EOF_

	$WPA_BIN -i $IF -B -c $WPA_CONF
}


stop_ap() {
	echo "Stopping Access Point on $IF"
	cat > $WPA_CONF <<-_EOF_
	ctrl_interface=/var/run/wpa_supplicant
	ap_scan=1
	_EOF_

	killall $WPA_BIN
}

start_dhcp_srv() {
	echo "Starting dhcp server on $IF"
	cat > $DHCP_CONF <<-_EOF_
	default-lease-time 86400;
	max-lease-time 86400;

	subnet $DHCP_SUBNET netmask $DHCP_NETMASK {
		range $DHCP_CLI_IP_FIRST $DHCP_CLI_IP_LAST;
	}
	_EOF_

	/etc/init.d/$DHCP_SCRIPT restart
	if test $? -ne 0; then
		echo "Starting dhcp server failed"
		exit 0
	fi
}


stop_dhcp_srv() {
	echo "Stopping dhcp server on $IF"
	# This prevent daemon to start at boot
	rm -f $DHCP_CONF
	/etc/init.d/$DHCP_SCRIPT stop
	if test $? -ne 0; then
		echo "Stopping dhcp server failed"
	fi
}

stop_access_point()
{
    stop_ap
    stop_dhcp_srv
}


###############################################



configure_host()
{
	#echo "Configuring $IF as host"
    local prot=$1 
    local sid=$2
    local passphrase=$3
   
    cat > $WPA_CONF <<-_EOF_
		ctrl_interface=/var/run/wpa_supplicant
		ap_scan=1

		network={
		        ssid="$sid"
		        key_mgmt=$prot
		_EOF_
    
	case "$prot" in
	    WPA-PSK)
		cat >> $WPA_CONF <<-_EOF_
		    psk="$passphrase"
		_EOF_
		;;
	    WPA-EAP)
		test -n "$2" || print_help
		test -n "$3" || print_help

		cat >> $WPA_CONF <<-_EOF_
        		eap=PEAP
		        identity="$3"
			    password="$passpharase"
                phase1="peaplabel=0"
                phase2="auth=MSCHAPV2"
		_EOF_
		;;
	esac
    
    cat >> $WPA_CONF <<-_EOF_
    }
_EOF_
}

stop_host() {
	rm -f $WPA_CONF
	killall $WPA_BIN
    rm -f /var/run/wpa_supplicant/wlan0
    /sbin/ifdown $IF
}


wait_if_ready()
{
	local i=0;

	read status < /sys/class/net/$IF/operstate
	while test $status != "up";do
		i=$((i + 1))
		if test $i -gt 10;then
			echo "timeout waiting for $IF to become ready"
			exit 1
		fi
		sleep 1
		read status < /sys/class/net/$IF/operstate
	done
}

configure_ap()
{
	if test -n "$1"; then
		start_ap "$1" "$2" "$3"
		wait_if_ready
		ifconfig $IF $DHCP_SRV_IP
		wait_if_ready
		start_dhcp_srv
	else
		stop_ap
		wait_if_ready
		stop_dhcp_srv
	fi
}


################################################################





##
# Parse all config (ini) file key and gets  its value.
# Keys are stored as space separated string named wordlist
#
parse_conf()
{
    local conf=$1 ; shift 
    local wlist=$@

    for word in ${wordlist} ; do
        value=`/bin/grep "^${word}" ${conf} |/bin/sed 's/.*=\([^=]*\)/\1/g')`
        eval ${word}="'$value'"
        #eval echo "\$$word"
    done
}



read -r -d '' wordlist <<'EOF'
enable 
channel 
mode 
passphrase 
ssid 
standard
EOF

iface=$(grep -o "wlan[0-9]" /etc/network/interfaces)


##
# config wifi based on mode type: managed or access point
#
wifi_config() 
{
    if [ $enable == "0" ]; then
        echo "$IF disabled"
        return 1
    fi

    [ -n "$ssid" ] || return 2            
    [ -z "$passphrase" ] && WPA_PROTOCOL="NONE"

    case ${mode} in
        managed)
            start_host ${WPA_PROTOCOL} ${ssid} ${passphrase}
            [ -n "$iface" ] && /sbin/ifup $iface
        ;;
        access_point)
            configure_ap ${ssid} ${channel} ${passphrase}
        ;;
        *)
            return 3
    esac
}


##
# general main.
#
case "$1" in
    ap)
		echo -n "Password for $2:"
		read -s passphrase
		echo ""
        configure_ap "$2" "$3" "${passphrase}"
        ;;
    host)
        passphrase="$4"
        key_mgm="$2"
        [ -z "$passphrase" ] && key_mgm="NONE"
        configure_host "$key_mgm" "$3" "${passphrase}"
        ;;
    help)
        print_help
        ;;
    start)
        [ -e $CONF_FILE ] || exit 5
        parse_conf ${CONF_FILE} ${wordlist}
        wifi_config
        ;;
    stop)
        # do not care too much about stop !!!
        # # then read agein config file and stop
        [ -e $CONF_FILE ] || exit 5
        parse_conf ${CONF_FILE} ${wordlist}
        [ "$mode" == "access_point" ] && stop_access_point
        [ "$mode" == "managed" ] && stop_host
        ;;
    *)
        print_help
        ;;
esac

exit $?
