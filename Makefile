CC=arm-linux-gcc
LD=arm-linux-gcc


END=
PRJ=iwscan

DEFINE_OPTS=

# C Compiler flags
CFLAGS=  -g  -MMD -MP -MF "$@.d" $(DEFINE_OPTS) -D_GNU_SOURCE -DLOG_USE_COLOR #-O3

# Linker options
LDFLAGS= -liw -linotifytools -lpthread -lwpa_client -lssl -lcrypto -lqrencode

IUSER ?= root
SCP    ?= scp
IHOST ?= 192.168.1.21
IPATH ?= /root

CURRDIR?=$(PWD)
PACKAGE_DIR=$(CURRDIR)/package
OPKG_DIR=$(CURRDIR)/opkgbin

INCLUDE-DIRS=

# source file to be compiled
SRC:= main.c 		\
	dump.c			\
	wifi.c			\
	ioblock.c		\
	aplist.c		\
	parameters.c	\
	netinfo.c		\
	ssl.c			\
	http.c			\
	wait.c			\
	log/log.c		\
	qrencode.c		\
	$(END)

# derive .o files
OBJS_C:=$(SRC:%.c=%.o)
OBJS:=$(OBJS_C:%.cpp=%.o)

MAJOR := `cat release.h | tr -s ' ' | grep '\#define RELEASE_MAJOR_NUMBER' | cut -d" " -f 3`
MINOR := `cat release.h | tr -s ' ' | grep '\#define RELEASE_MINOR_NUMBER' | cut -d" " -f 3`
PATCH := `cat release.h | tr -s ' ' | grep '\#define RELEASE_PATCH_NUMBER' | cut -d" " -f 3`
RELEASE := $(MAJOR).$(MINOR).$(PATCH)

.PHONY: clean install all print package pkg-install

all: $(PRJ)

install: $(PRJ)
	$(SCP) $(PRJ) $(IUSER)@$(IHOST):$(IPATH)


package: $(PRJ)
	@$(RM) -rf $(OPKG_DIR)/*
	@mkdir -p $(PACKAGE_DIR)/CONTROL
	@sed -r "s/Version:(.*)/Version: $(RELEASE)/g" opkg/control > $(PACKAGE_DIR)/CONTROL/control
	@mkdir -p $(PACKAGE_DIR)/usr/bin
	@cp -a opkg/etc $(PACKAGE_DIR)/
	@cp -a ./$(PRJ) $(PACKAGE_DIR)/usr/bin
	chmod 755 $(PACKAGE_DIR)/usr/bin/$(PRJ)
	@cp -a opkg/scripts/* $(PACKAGE_DIR)/CONTROL/
	@mkdir -p $(OPKG_DIR)
	opkg-build -o root -g root $(PACKAGE_DIR) $(OPKG_DIR) 
	opkg-make-index $(OPKG_DIR) > $(OPKG_DIR)/Packages
	@$(RM) -f $(OPKG_DIR)/Packages.gz
	@gzip $(OPKG_DIR)/Packages
	@sed -r  "s/Installed-Size:(.*)/Installed-Size: `stat -L -c%s "$(PRJ)"`/g" ./status.template > ./status
	@sed -r -i  "s/Version:(.*)/Version: $(RELEASE)/g" ./status

opkg-install: package
	$(SCP) -i ~/.ssh/microcamera.pem $(OPKG_DIR)/$(PRJ)_$(RELEASE)_arm.ipk  root@microcamera.cloud:/var/www/html/upgrade
	$(SCP) -i ~/.ssh/microcamera.pem $(OPKG_DIR)/Packages.gz  root@microcamera.cloud:/var/www/html/upgrade


$(PRJ): $(OBJS)
	@echo -n  "LD $@: "
	$(LD) -o $@ $^ $(LDFLAGS) 
	@echo "OK"


# Clean Targets
clean:
	@$(RM) $(OBJS) *~ $(addsuffix .d, $(OBJS)) $(PRJ) status
	@echo "Removed obj files, and ~ .a, $(PRJ) and opk package"


print:
	@echo $(OBJS)
	@echo "$(IUSER)@$(IHOST):$(IPATH)"
	@echo $(DEPS)
	@echo "$(RELEASE)"

# Compile rules
%.o: %.c
	@echo -n  "CC $<: "
	$(RM) "$@.d"
	$(CC) -c  $(CFLAGS)  $(INCLUDE-DIRS) -o $@ $<
	@echo "OK"

# Dependencies
DEPS=$(wildcard $(addsuffix .d, $(OBJS)))

ifneq ($(DEPS),)
include $(DEPS)
endif

ifndef VERBOSE
.SILENT: 
endif
