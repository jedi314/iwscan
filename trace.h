/** 
 \file trace.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>

 \brief Debug functions
*/


#ifndef _TRACE_H
#define _TRACE_H

#include "log/log.h"

#define DEBUG_PRINT(fmt, args...) printf("DEBUG: %s:%d:%s: " fmt, \
    __FILE__, __LINE__, __PRETTY_FUNCTION__ , ##args)


#define TRACE log_trace("%s\n", __PRETTY_FUNCTION__)

void dump(char *desc, void *addr, int len);
#endif
