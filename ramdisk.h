/** 
 \file ramdisk.h
 \author Giandomenico Rossi <g.rossi@tattile.com>

 \brief
*/


#ifndef _RAMDISK_H
#define _RAMDISK_H

#include "wifi.h"

#define BLK_SIZE 512
#define N_BLOCK_IN 8
#define N_BLOCK_OUT 8
#define IN_BUFFER_SIZE BLK_SIZE * N_BLOCK_IN
#define IN_BUFFER_OFFSET 0

#define OUT_BUFFER_SIZE BLK_SIZE * N_BLOCK_OUT
#define OUT_BUFFER_OFFSET IN_BUFFER_OFFSET + IN_BUFFER_SIZE

#define IN_DATA_BLOCK 16
#define N_IN_DATA_BLOCK 32768
#define IN_DATA_OFFSET IN_DATA_BLOCK * BLK_SIZE 
#define IN_DATA_SIZE N_IN_DATA_BLOCK * BLK_SIZE

#define OUT_DATA_BLOCK 32784
#define N_OUT_DATA_BLOCK 32768
#define OUT_DATA_OFFSET OUT_DATA_BLOCK * BLK_SIZE
#define OUT_DATA_SIZE N_OUT_DATA_BLOCK * BLK_SIZE


#define ABORT_KEY 0x13ab87f4


enum command_state {
    EXECUTING = 0,
    OK,
    ABORTED,
    TERMINATED_WITH_ERROR,
};


struct command_in {
    uint16_t  id;
    uint16_t  n_param;
    uint32_t  seq;
    uint8_t   params[IN_BUFFER_SIZE];
};

struct command_out {
    uint16_t  id;
    uint16_t  n_param;
    uint32_t  seq;
    uint16_t  status;
    uint16_t  error;
    uint8_t   params[OUT_BUFFER_SIZE];
};


struct ramdisk_blk {
    uint16_t id;
    uint16_t offset;
    uint32_t size;
    uint8_t  *buffer;
};

struct release {
    union{
        struct {
            uint8_t patch;
            uint8_t minor;
            uint8_t major;
            uint8_t pad;
        };
        uint32_t version;
    };
};



struct ramdisk {
    char* name;
    int fd;
    uint32_t last_cmd;
    uint32_t cmd_ready;
    int thread;
    uint8_t *indata;             /*!< mmaped address of input data block    */
    uint8_t *outdata;            /*!< mmaped address of output data block   */ 
    struct release release;
    struct ramdisk_blk block;    /*!< current data block buffer */
    struct command_ops *cmd_ops; /*!< pointer to commands ensamble  */
    struct command_in *in;       /*!< pointer to command input struct */
    struct command_out *out;     /*!< pointer to command input struct */
    struct wifi_list wifi;       /*!< list of wifi ap  */
};




#endif


