/** 
 \file wifi.h
 \author Giandomenico Rossi <g.rossi@tattile.com>

 \brief
*/


#ifndef _WIFI_H
#define _WIFI_H

#include <iwlib.h>
#include <wpa_ctrl.h>


struct ap_info{
    uint8_t mac[6];
    char ssid[64];
    uint32_t channel;
    int32_t level;
    int32_t quality;
    uint32_t enc;
};




struct wifi_list{
    pthread_t wifi_scan;
    char* iface;
    char* wpa_path;
    char* list;
    int active_ap;
    int n_ap;
    //struct tpc_connection conn;
    //struct ssl_session sslsec;
    struct ap_info *ap_info;
    struct wpa_ctrl* wpa;    
    struct wireless_scan *former;
    struct wireless_scan *last;
    pthread_mutex_t lock;
    
};


struct wifi_status {
    int connection;
    int quality;
    int channel;
};


struct wifi_enc {
    union {
        struct {
            uint32_t wpa2:1;
            uint32_t wpa:1;
            uint32_t wep:1;
            uint32_t ess:1;
            uint32_t p2p:1;
            uint32_t psk:1;
            uint32_t eap:1;
            uint32_t ccmp:1;
            uint32_t tkip:1;
            uint32_t aes:1;
        };
        uint32_t mask;
    };
};




enum wifi_errors {
    WIFI_OK,
    WIFI_NO_AP=30,
    WIFI_CONNECTION_ERROR,
    ADD_NETWORK_ERR,
    SET_NETWORK_ERR,
    ENABLE_NETWORK_ERR,
    DISABLE_NETWORK_ERR,
    WIFI_STATUS_ERR,
    WIFI_RSS_ERR,
    SIGNAL_POLL_ERR,
    STATUS_NETWORK_ERR,
    RECONNECT_NETWORK_ERR,
    WIFI_SOCKET_ERROR,
};

int wifi_init(struct wifi_list* w, int run_thread);
int wifi_connect(struct wifi_list* wifi, char* ssid, char* key);
int free_results(wireless_scan *org);
struct ap_info* look_for_essid(struct wifi_list* wifi, char* essid);
int wifi_disconnect(struct wifi_list* wifi);
int get_dhcp(char *iface);
int wifi_status(struct wifi_list* w, struct wifi_status *status);
struct ap_info* scan_wifi_ap(struct wifi_list *wifi, int* n_ap);
int wifi_is_connected(struct wifi_list *w);



#endif
