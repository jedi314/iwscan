/** 
 \file http.h
 \author Giandomenico Rossi <g.rossi@tattile.com>
 

 \brief
*/


#ifndef _HTTP_H
#define _HTTP_H

#include <netinet/in.h>
#include <sys/socket.h>
#include "ssl.h"

#define HTTP_TIMEOUT 1

struct http {
    char hostname[128];
    int port;
    uint8_t  *data;
    struct tcp_connection connection;
    uint32_t timeout;
};

struct https{
    struct http http;
    struct ssl_session session;
};


enum http_errors {
    HTTP_OK,
    HTTP_CONNECTION_ERR=10,
    HTTP_WAIT_TIMEOUT,    
    HTTP_UPDATE_TIMEOUT,
    HTTPS_READ_ERROR,
    HTTPS_WANT_READ_ERROR,
    HTTPS_WANT_WRITE_ERROR,
    HTTPS_ZERO_RETURN_ERROR,
};


#define UPDATE_N_BLOCKS (96)

int http_open(struct http* http);
void http_close(struct http* http);
void https_close(struct https *https);
int https_write(struct https *post, int size);
int https_open(struct https* https);
int https_read(struct https *post, int chunk);
int get_content_length(char* header);
int get_header_length(char* header);

#endif
