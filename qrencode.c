/** 
 \file qrencode.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 \copyright 2022 APR

 

 \brief
*/

#include <stdio.h>

#include "log/log.h"
#include "qrencode.h"

struct data_type {
    int data;
    int number;
    int alphanum;
    int binary;
};

struct qr_module {
    int version;
    int width;
    struct data_type l;
    struct data_type m;
    struct data_type q;
    struct data_type h;
};









QRcode* encode_qr(char* str, QRecLevel level, int version)
{
    QRcode* qrc;

    qrc = QRcode_encodeString8bit(str, version, level);
    if(qrc == NULL){
        log_error("during cretion of QRCode object\n");
        return NULL;
    }
    log_info("QR Width: %d\n", qrc->width);

    return qrc;
}


void dump_modules(QRcode* qrc)
{
    int i,j;
    char b;

    if(current_log_level() > LOG_INFO)
        return;
    
    for(i=0; i<qrc->width; i++){
        for(j=0; j<qrc->width; j++){
            if((1 & qrc->data[i*qrc->width+j]) == 0)
                b='#';
            else
                b=' ';
            printf("%c%c", b, b);
        }
        printf("\n");
    }
}


void copy_modules(QRcode* qrc, char* addr)
{
    int i ;
    int length = qrc->width*qrc->width;

    for(i=0; i<length; i++)
        addr[i] = qrc->data[i] & 0x1 ;

}

int accepted_qr_level(int level)
{
    return level >=	QR_ECLEVEL_L && level <= QR_ECLEVEL_H ;
}


void free_qr(QRcode* qrc)
{
    QRcode_free(qrc);    
}
