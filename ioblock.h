/** 
 \file ioblock.h
 \author Giandomenico Rossi <g.rossi@tattile.com>
 \copyright 2021 Tattile s.r.l.
 

 \brief
*/

#ifndef _IOBLOCK_H
#define _IOBLOCK_H

#include "ramdisk.h"

int open_ramdisk(struct ramdisk *rmd);
int read_rmd(struct ramdisk *rmd, int block);
off_t moveto (int fd, off_t off);
void* rmd_mmap(struct ramdisk* rmd, uint32_t blkid, int size);
struct parameter* increment_param(struct parameter* pin);
int write_pad_array(struct parameter* pin, uint8_t *p, int n, int pad);
int write_array_param(struct parameter* pin, uint8_t *p, int n);
int write_str_param(struct parameter* pin, char* str);


#endif
