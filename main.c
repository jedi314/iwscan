/** 
 \file main.c
 \author Giandomenico Rossi <gdrossi@hotmail.com>

 \brief 
*/

#include <stdio.h>
#include <time.h>
#include <errno.h>
#include <inotifytools/inotifytools.h>
#include <inotifytools/inotify.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/fs.h>
#include <sys/types.h>
#include <regex.h>
#include <sys/mman.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <string.h>
#include <stdlib.h>

#include "release.h"
#include "trace.h"
#include "ramdisk.h"
#include "ioblock.h"
#include "aplist.h"
#include "parameters.h"
#include "log/log.h"
#include "qrencode.h"

#define EVENT_SIZE  ( sizeof (struct inotify_event) )
#define EVENT_BUF_LEN     ( 1024 * ( EVENT_SIZE + 16 ) )

#define LOG_USE_COLOR 1

#define call_ops(cops, op, args...)             \
    ((cops)->op ? (cops)->op(args):0 )       

enum blk_area {
    MC_COMMAND = 0,
    PI_RESPONSE = 8,
};


enum command_id {
    SCAN = 1,
    CONNECT=2,
    DISCONNECT=3,
    STATUS=4,
    SEND=5,
    SECURE_SEND=7,
    INFO=9,
    QR_ENCODE=10,
    RELEASE=11,
    UPGRADABLE=12,
    UPGRADE=13,
    N_COMMANDS,
};


struct command_ops {
    enum command_id id;
    int (*execute)(struct command_in* cmdin, struct command_out* cmdout, void* param );
    int (*response)(void* block);
};


int get_wifi_ap(struct command_in* cmdin, struct command_out* cmdout, void* param);


struct command_ops ops[] = {
    { 0 , 0 , 0 },
    { SCAN, get_wifi_ap, NULL },
    { CONNECT, connect_to_ap, NULL },
    { DISCONNECT, remove_to_ap, NULL },
    { STATUS, connection_status, NULL },
    [7] = {SECURE_SEND, https_send, NULL },
    [9] = { INFO, get_network_info, NULL },
    [10] = { QR_ENCODE, qr_encode, NULL },
    [11] = { RELEASE, get_release, NULL },
    [12] = { UPGRADABLE, upgradable_release, NULL },
    [13] = { UPGRADE, upgrade_release, NULL },
    { -1 , 0, 0 }
};

/*  */


int notify_event(char* filename, int event)
{
    /* initialize and watch the entire directory tree from the current working
	   directory downwards for all events */
    if ( !inotifytools_initialize() ) {
		fprintf(stderr, "%s\n", strerror( inotifytools_error() ) );
        return -1;
    }
    
    if(!inotifytools_watch_recursively(filename , event)) {
        fprintf(stderr, "%s\n", strerror( inotifytools_error() ) );
        return -2;
    }
    
    return 0;
}

int cmd_in_range( struct command_in* cmd )
{
    return cmd->id > 0 && cmd->id < N_COMMANDS;
}
                  

int parse_command(struct ramdisk *rmd)
{
    int id, n, ret = 0;
    struct command_in* cmdin = rmd->in;
    struct command_out* cmdout= rmd->out;
    struct command_ops *cmd_ops = rmd->cmd_ops;
    
    id = cmdin->id;
    if ( cmd_in_range(cmdin) ){
        n = call_ops(&ops[id], execute, cmdin, cmdout, rmd);
        if(n < 0) {
            cmdout->status = TERMINATED_WITH_ERROR;
            cmdout->error = -n;
            cmdout->n_param = 0;
        } else {
            cmdout->n_param = n;
            cmdout->error = 0;
            cmdout->status = OK;            
        }
        rmd->last_cmd = cmdin->seq;
        if (id == 13)           /* update */
            exit(99);
        return n;
    } else {
        printf("NO COMMAND [%d]\n", cmdin->id);
        return -1; 
    }

    return 0;
}



int is_new_command(struct ramdisk* rmd)
{
    return rmd->in->seq > rmd->last_cmd;
}

void ready_for_command(struct ramdisk* rmd, enum command_state state)
{
    rmd->cmd_ready = OK;
    rmd->last_cmd  = rmd->in->seq;
}


void busy_for_command(struct ramdisk* rmd)
{
    rmd->cmd_ready = EXECUTING;
    rmd->out->id = rmd->in->id;
    rmd->out->n_param = 0;
    rmd->out->seq = rmd->in->seq;
    rmd->out->status = EXECUTING;
}



int main(int argc, char** argv)
{
    int ret=0;
    struct inotify_event * event;
    enum command_state status;
    int c = 0;
    int opt;
    int log_level= LOG_FATAL;
    
    struct ramdisk ramd = {
        .name = "/dev/ram0",
        .cmd_ops = ops,
        .last_cmd = 0,
        .thread = 0,
        .wifi = {
            .wpa_path = "/var/run/wpa_supplicant/wlan0",
            .iface = "wlan0",
            .ap_info = NULL,
            .active_ap = -1,
        },
        .release = {
            .major = RELEASE_MAJOR_NUMBER,
            .minor = RELEASE_MINOR_NUMBER,
            .patch = RELEASE_PATCH_NUMBER,
            .pad = 0,
        },
    };
    
    printf("%s - relese: %d.%d.%d\n",
           argv[0], ramd.release.major, 
           ramd.release.minor,
           ramd.release.patch);    

    
    
    
    while ((opt = getopt(argc, argv, "vl:dt")) != -1) 
    {
        switch (opt) {
        case 'v':
            exit(0);
        case 'l':
            log_level = get_log_level(optarg);
            break;
        case 'd':
            log_level = get_log_level("DEBUG");
        case 't':
            log_level = get_log_level("TRACE");
            
        }
    }

    
    log_set_level(log_level);
    
    wifi_init(&ramd.wifi, ramd.thread);
    
    ret = open_ramdisk(&ramd);
    if(ret < 0)
        return ret;

    /* map in commad block */
    ramd.in = rmd_mmap(&ramd, IN_BUFFER_OFFSET, IN_BUFFER_SIZE);
    if(ramd.in == MAP_FAILED)
        return -1;

    
    ramd.out = rmd_mmap(&ramd, OUT_BUFFER_OFFSET, OUT_BUFFER_SIZE);
    if(ramd.in == MAP_FAILED)
        return -1;


    ramd.indata = rmd_mmap(&ramd, IN_DATA_OFFSET, IN_DATA_SIZE);
    if(ramd.indata == MAP_FAILED)
        return -1;

    ramd.outdata = rmd_mmap(&ramd, OUT_DATA_OFFSET, OUT_DATA_SIZE);
    if(ramd.outdata == MAP_FAILED)
        return -1;

    ramd.in->id = 0;
    
    notify_event("/dev/ram0", IN_MODIFY);

	while (1) {
        event = inotifytools_next_events( -1, 1 );
		//inotifytools_printf( event, "%T %w%f %e\n" );
        if( is_new_command(&ramd) ) {
            //printf("CMD: %u\n", ramd.in->id);
            busy_for_command(&ramd);
            status = parse_command(&ramd);
            ready_for_command(&ramd, status);
        } else if(ramd.in->seq < ramd.last_cmd) {
            printf("seq: %d last: %d\n", ramd.in->seq , ramd.last_cmd );
            printf("NO NEW COMMAND\n");
        }
	}
    
    return 0;
}


