/** 
 \file wifi.c
 \author Giandomenico Rossi <g.rossi@tattile.com>

 \brief 
 There are a thread which scan every scan_time ms
 looking for all access point availabele.

*/

#include <pthread.h>
#include <stdlib.h>
#include <sys/param.h>
#include <unistd.h>

#include "trace.h"
#include "wifi.h"
#include "log/log.h"

#define WIFI_SCAN_TIME 3



struct enc_type{
    char* name;
    int size;
    int mask; 
};

static struct enc_type enc[] = {
    { "WPA2", 4 , 0},
    { "WPA" , 3 , 1},
    { "WEP" , 3 , 2},
    { "ESS" , 3 , 3},
    { "P2P" , 3 , 4},
    { "PSK" , 3 , 5},
    { "EAP" , 3 , 6},
    { "CCMP", 4 , 7},
    { "TKIP", 4 , 8},
    { "AES" , 3 , 9},
    
    { 0, 0}
};



int wifi_cmd(struct wifi_list* wifi, char* cmd, char* ans, int ans_len)
{
    int err;
    int len;
    
    len = strlen(cmd);
    err = wpa_ctrl_request(wifi->wpa, cmd, len, ans, &ans_len, NULL);
    if(err)
        return err;
    ans[ans_len] = 0;
    
    return 0;
}


static
int get_mac(char* s, uint8_t* mac)
{
    int n;
    char* bssid ;

    bssid = strtok(s, "\t");
    n = sscanf(bssid, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx",
               &mac[0],
               &mac[1],
               &mac[2],
               &mac[3],
               &mac[4],
               &mac[5]);

    return n;
}

static
uint32_t get_frequency(void)
{
    char *fq;
    uint32_t f ;
    
    fq = strtok(NULL, "\t");
    sscanf(fq, "%u", &f);

    return f;
}

static
int32_t get_level(void)
{
    char *level;
    uint32_t l ;
    
    level = strtok(NULL, "\t");
    sscanf(level, "%d", &l);

    return l;    
}



static 
int parse_encryption(char* str)
{
    char *p = str;
    char *ptk;
    int i, mask;
    
    mask = 0;
    while( ( ptk = strsep(&p, "-+") ) != NULL ) {
        for(i=0; enc[i].name; i++){
            if(strncmp(ptk, enc[i].name, enc[i].size ) == 0){
                mask |= 1 << enc[i].mask ;
                break;
            }
        }
    }
    return mask;
}   

static
int enc_type(struct wifi_enc* we)
{
    int enc = 0;

    if(we->ess)
        enc = 0;
   
    if(we->ess && we->wep)
        enc = 1;

    if(we->wep)
        enc = 2;

    if(we->wpa && we->psk && we->tkip)
        enc = 3;

    if(we->wpa && we->psk && we->aes)
        enc = 4;

    if(we->wpa2 && we->psk && we->tkip)
        enc = 5;

    if(we->wpa2 && we->psk && we->aes)
        enc = 6;
    
    if(we->wpa2 && we->psk && we->ccmp)
        enc = 7;

    return enc;
}

static
int get_encryption(void)
{
    char *enc;
    struct wifi_enc wenc;
    char* tk;
    
    wenc.mask = 0;
    enc = strtok(NULL, "\t");
    tk=strsep(&enc, "]");
    if (tk != NULL && *tk != 0) {
        tk++;
        wenc.mask = parse_encryption(tk);
    }
    return enc_type(&wenc);
}

static
int get_channel(int freq)
{

    
    if (freq >= 2412 && freq <= 2484) {
        return (freq - 2412) / 5 + 1;
    } else if (freq >= 5170 && freq <= 5825) {
        return (freq - 5170) / 5 + 34;
    } else {
        return -1;
    }
}


int fq_to_channel(int freq)
{
    /* see 802.11 17.3.8.3.2 and Annex J */
    if (freq == 2484)
        return 14;
    else if (freq < 2484)
        return (freq - 2407) / 5;
    else if (freq >= 4910 && freq <= 4980)
        return (freq - 4000) / 5;
    else if (freq < 5925)
        return (freq - 5000) / 5;
    else if (freq == 5935)
        return 2;
    else if (freq <= 45000) /* DMG band lower limit */
        /* see 802.11ax D6.1 27.3.22.2 */
        return (freq - 5950) / 5;
    else if (freq >= 58320 && freq <= 70200)
        return (freq - 56160) / 2160;
    else
        return 0;
}


static
void get_ssid(char* ssid)
{
    char *s;
    
    s = strtok(NULL, "\t");
    
    if(s == NULL){
        ssid[0] = 0;
        return ;
    }
    
    strcpy(ssid, s);
}

int32_t get_quality(int32_t level)
{
    return MIN(MAX(2 * (level + 100), 0), 100);
}


static
int write_ap_data(char* list, struct ap_info **api)
{
    uint8_t mac[6];
    char ssid[64];
    char *l = NULL, *line;
    uint32_t frequency, level, enc;
    int n = 0;
    struct ap_info *ap=NULL;
    int i, fq;
    
    line = strsep(&list, "\n");
    while( (line = strsep(&list, "\n")) != NULL) {
        if( *line == 0 )        /* delimiter */
            continue;
        ap = reallocarray(ap, n+1, sizeof(struct ap_info));
        get_mac(line, ap[n].mac);
        fq = get_frequency();
        ap[n].channel = fq_to_channel(fq);
        ap[n].level = get_level();
        ap[n].enc = get_encryption();
        get_ssid(&ap[n].ssid[0]);
        ap[n].quality = get_quality(ap[n].level);
        n++;
    }
    
    *api = ap;
    return n;
}


static
char* str_tok(char* buf, char* needle)
{
    char* wpa_state;
    int len;   
    char ret ;
    
    len = strlen(needle);
    wpa_state = strstr(buf, needle);
    if(wpa_state == NULL)
        return NULL;

    wpa_state += len;

    return wpa_state;
}
static
int is_connected(char* buf)
{
    char needle[] = "wpa_state=";
    char* wpa_state;
    char ret ;

    wpa_state = str_tok(buf, needle);
    if(wpa_state == NULL)
        return -WIFI_STATUS_ERR;

    log_info("is_connected Status: %s\n", wpa_state);
    return strncmp(wpa_state, "COMPLETED", 9) == 0 ? 1 : 0 ;
}


static
int get_rssi(char* buf)
{
    char needle[] = "RSSI=";
    char* wpa_state;
    int rssi;
    
    wpa_state = str_tok(buf, needle);
    if(wpa_state == NULL)
        return -WIFI_RSS_ERR;
    
    sscanf(wpa_state, "%d", &rssi);

    return rssi;
}

static
int get_current_fq(char* buf)
{
    char needle[] = "FREQUENCY=";
    char* wpa_state;
    int freq;
    
    wpa_state = str_tok(buf, needle);
    if(wpa_state == NULL)
        return -WIFI_RSS_ERR;

    sscanf(wpa_state, "%d", &freq);

    return freq;
}


static int level_great(const void *a, const void *b)
{
    return ((struct ap_info*)a)->level > ((struct ap_info*)b)->level ? -1:1;
}

struct ap_info* scan_wifi_ap(struct wifi_list *wifi, int *n_ap)
{
    char* buf;
    int err, i ;
    size_t buf_len, len = 4096;
    struct ap_info *api;
    
    buf = malloc(len);
    buf_len = len ;

    
    for(i=0 ; i<3 ; i++){
        err = wpa_ctrl_request(wifi->wpa, "SCAN", 4, buf, &buf_len, NULL);
        buf[buf_len] = 0 ;
        if(err != 0)
            log_debug("%s [%d]\n", buf, err);
        buf_len = 0;
        buf_len = len ;
        err = wpa_ctrl_request(wifi->wpa, "SCAN_RESULTS", 12, buf, &buf_len, NULL);
        buf[buf_len] = 0 ;
        sleep(1);
    }
    
    log_debug("\n%s\n [%d]{%d}\n\n", buf, i, buf_len);
    *n_ap = write_ap_data(buf, &api);
    qsort(api, *n_ap, sizeof(struct ap_info), level_great);

    free(buf);
    
    return api;
}

void wifi_scan_ap(struct wifi_list *wifi)
{
    int n_ap;
    struct ap_info *api = NULL;
    int n_try = 0;

    for(n_try=0; n_try < 3; n_try++){
        api = scan_wifi_ap(wifi, &n_ap);
        if (api != NULL){
            wifi->ap_info = api;
            wifi->n_ap = n_ap;
            break;
        }
        sleep(1);
    }
}

static
void* _scan_wifi_ap(void* param)
{
    struct wifi_list *wifi = (struct wifi_list*)param;
    struct ap_info *api, *old;    
    int n_ap;
    
    while(1) {
        api = scan_wifi_ap(wifi, &n_ap);
        
#if 0
        for (i=0; i<n_ap; i++){
            log_info("%s %d %d\n", api[i].ssid, api[i].level, api[i].quality);
        }
#endif
        
        pthread_mutex_lock(&wifi->lock);
        old = wifi->ap_info;
        wifi->ap_info = api;
        wifi->n_ap = n_ap;
        pthread_mutex_unlock(&wifi->lock);

        free(old);
        sleep(WIFI_SCAN_TIME);
    }
    
    pthread_exit(NULL);
}

struct ap_info* look_for_essid (struct wifi_list* wifi, char* essid)
{
    int i;
    struct ap_info *ap = wifi->ap_info;
    int n = wifi->n_ap;
    
    if (ap == NULL){
        return NULL;
    }

    for(i=0; i<n; i++){
        if(strcmp(ap[i].ssid, essid) == 0)
            return &ap[i];
    }
    
    return NULL;
}

int get_dhcp(char *iface)
{
    char str[64];
    int err;
    
    err = system("/sbin/udhcpc -R -i wlan0 -n");
    log_debug("dhcp: err=%d\n", err);
    if (err)
        err = -WIFI_CONNECTION_ERROR;
    
    return err;
}


static
int wifi_remove_networks(struct wifi_list* wifi)
{
    int err;
    char buf[256], bufin[256];
    int i, n, buf_len = 256;
    int fail = 0;
    
    for(i = 0; fail == 0 && i< 10; i++){
        
        n = sprintf(bufin, "REMOVE_NETWORK %d", i);
        err = wpa_ctrl_request(wifi->wpa, bufin, n, buf, &buf_len, NULL);
        buf[buf_len] = 0;     
        if (err)
            return -DISABLE_NETWORK_ERR;

        if(strncmp(buf, "FAI", 3) == 0){
            break;
        }
    }

    return i;
}



static
int wifi_disable_networks(struct wifi_list* wifi)
{
    int err;
    char buf[256], bufin[256];
    int i, n, buf_len = 256;
    int fail = 0;
    
    for(i = 0; fail == 0 && i< 10; i++){
        
        n = sprintf(bufin, "DISABLE_NETWORK %d", i);
        err = wpa_ctrl_request(wifi->wpa, bufin, n, buf, &buf_len, NULL);
        buf[buf_len] = 0;     
        if (err)
            return -DISABLE_NETWORK_ERR;

        if(strncmp(buf, "FAI", 3) == 0){
            break;
        }
    }

    return i;
}


int wifi_disconnect(struct wifi_list* wifi)
{
    int err;
    char buf[256], bufin[256];
    int n, buf_len = 256;

    err = wpa_ctrl_request(wifi->wpa, "DISCONNECT", 10, buf, &buf_len, NULL);
    if (err)
        return -DISABLE_NETWORK_ERR;

    wifi_disable_networks(wifi);
    wifi_remove_networks(wifi);
    /* n = sprintf(bufin, "REMOVE_NETWORK %d", wifi->active_ap); */
    /* err = wpa_ctrl_request(wifi->wpa, bufin, n, buf, &buf_len, NULL); */
    /* if (err) */
    /*     return -DISABLE_NETWORK_ERR; */
    
    system("killall -q udhcpc");
    wifi->active_ap = -1;
    
    return WIFI_OK;
}



int wifi_connect(struct wifi_list* wifi, char* ssid, char* key)
{
    int err = 0;
    char buf[256], bufin[256];
    size_t n, buf_len = 256;
    int id;
    
    wifi->active_ap = -1;
    log_trace("0. %s\n", "ADD_NETWORK");
    err = wpa_ctrl_request(wifi->wpa, "ADD_NETWORK", 11,  buf, &buf_len, NULL);
    if(err)
        return -ADD_NETWORK_ERR;
    buf[buf_len] = 0;
    sscanf(buf, "%d\n", &id);
    
    n = sprintf(bufin, "SET_NETWORK %d ssid \"%s\"", id, ssid);
    log_trace("1. %s\n", bufin);
    err = wpa_ctrl_request(wifi->wpa, bufin, n, buf, &buf_len, NULL);
    if(err)
        return -SET_NETWORK_ERR;
    buf[buf_len] = 0;

    if(key[0] == 0){
        n = sprintf(bufin, "SET_NETWORK %d key_mgmt NONE", id);
    } else {
        n = sprintf(bufin, "SET_NETWORK %d psk \"%s\"", id, key);
    }
    err = wpa_ctrl_request(wifi->wpa, bufin, n,  buf, &buf_len, NULL);
    if(err)
        return -SET_NETWORK_ERR;
    log_trace("2. %s == %s\n", bufin, key);
    log_trace("2.5 %s\n", bufin);
    n = sprintf(bufin, "ENABLE_NETWORK %d", id);
    log_trace("3. %s\n", bufin);
    err = wpa_ctrl_request(wifi->wpa, bufin, n,  buf, &buf_len, NULL);
    if(err)
        return -ENABLE_NETWORK_ERR;
    
    err = wpa_ctrl_request(wifi->wpa, "RECONNECT", 9,  buf, &buf_len, NULL);
    log_trace("4. %s\n", "RECONNECT");
    if(err)
        return -RECONNECT_NETWORK_ERR;
    wifi->active_ap = id;
    
    return WIFI_OK;
}




static void ifup_wlan()
{
    system("ifup wlan0");
}

int wifi_init(struct wifi_list* w, int run_thread)
{
    struct wpa_ctrl* wpa;

    
    wpa = wpa_ctrl_open(w->wpa_path);
    if(wpa == NULL){
        log_debug("%s [%s]\n", "Error open wpa ctrl interface", w->wpa_path);
        exit(-2);
    }
    w->wpa = wpa;
        

    if(run_thread){
        pthread_mutex_init(&w->lock, NULL);
        pthread_create(&w->wifi_scan, NULL, _scan_wifi_ap, w);
    }else {
        wifi_scan_ap(w);
    }
}



int wifi_status(struct wifi_list* w, struct wifi_status *status)
{
    char buf[256];
    int n, buf_len = 256;
    int err, fq;
    int rssi;

    err = wpa_ctrl_request(w->wpa, "STATUS", 6, buf, &buf_len, NULL);
    if (err)
        return -STATUS_NETWORK_ERR;

    buf[buf_len] = 0;
    status->connection = is_connected(buf);
    if(status->connection < 0)
        return -STATUS_NETWORK_ERR;
    
    if(status->connection) {
        err = wpa_ctrl_request(w->wpa, "SIGNAL_POLL", 11, buf, &buf_len, NULL);
        if (err)
            return -SIGNAL_POLL_ERR;
        rssi = get_rssi(buf);
        status->quality = get_quality(rssi);
        
        fq = get_current_fq(buf);
        log_info("Frequency: %d\n", fq);
        status->channel = fq_to_channel(fq);
    }
    
    return WIFI_OK;
}




int wifi_is_connected(struct wifi_list *w)
{
    return w->active_ap != -1;
}

