/** 
 \file netinfo.c
 \author Giandomenico Rossi <g.rossi@tattile.com>

 \brief
*/

#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netinet/in.h>

#include "netinfo.h"



int get_dns_servers(struct net_info* ni)
{
	FILE *fp;
	char line[200], *p;
    int len, i=0;
    uint8_t *pdns;
    
	if((fp = fopen("/etc/resolv.conf", "r")) == NULL){
		printf("Failed opening /etc/resolv.conf file \n");
	}
	
	while(fgets(line, 200, fp))	{
		if(line[0] == '#') {
			continue;
		}
		if(strncmp(line, "nameserver", 10) == 0) {
			p = strtok(line, " ");
			p = strtok(NULL, " ");
            pdns = malloc(4);
            sscanf(p, "%hhd.%hhd.%hhd.%hhd",
                   &pdns[0],
                   &pdns[1],
                   &pdns[2],
                   &pdns[3]);
            ni->dns[i] = pdns;           
            i++;
		}
	}
    fclose(fp);
    
    return i;
}



int get_gateway(struct net_info *ni)
{
    long destination, gateway;
    char iface[IF_NAMESIZE];
    char buf[4096];
    FILE * file;

    memset(iface, 0, sizeof(iface));
    memset(buf, 0, sizeof(buf));

    file = fopen("/proc/net/route", "r");
    if (!file)
        return -1;

    while (fgets(buf, sizeof(buf), file)) {
        if (sscanf(buf, "%s %lx %lx", iface, &destination, &gateway) == 3) {
            if (destination == 0) { /* default */
                fclose(file);
                *((struct in_addr *) &ni->gateway) = *((struct in_addr *) &gateway);
                return 0;
            }
        }
    }

    /* default route not found */
    if (file)
        fclose(file);
    return -1;
}

int get_netinfo(char* iface, struct net_info *ni)
{
    int fd;
    unsigned char *mac;
	struct ifreq ifr;

    
    fd = socket(AF_INET, SOCK_DGRAM, 0);
	ifr.ifr_addr.sa_family = AF_INET;
	strncpy(ifr.ifr_name , iface , IFNAMSIZ-1);
	ioctl(fd, SIOCGIFADDR, &ifr);
    *((struct in_addr*)ni->addr) = ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr;

	//printf("Addr : %.2x:%.2x:%.2x:%.2xx\n" , ni->addr[0], ni->addr[1], ni->addr[2], ni->addr[3]);
    
	// printf("IP address of %s - %s\n" , iface , inet_ntoa(( (struct sockaddr_in *)&ifr.ifr_addr )->sin_addr) );
	ioctl(fd, SIOCGIFNETMASK, &ifr);
    *((struct in_addr*)ni->mask) = ((struct sockaddr_in *)&ifr.ifr_addr)->sin_addr;
	//printf("Netmask of %s - %s\n" , iface , inet_ntoa(( (struct sockaddr_in *)&ifr.ifr_addr )->sin_addr) );

	ioctl(fd, SIOCGIFHWADDR, &ifr);
	mac = (unsigned char *)ifr.ifr_hwaddr.sa_data;
    memcpy(ni->mac, mac, 6); 
	//printf("Mac : %.2x:%.2x:%.2x:%.2x:%.2x:%.2x\n" , mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
	close(fd);

}
