/** 
 \file http.c
 \author Giandomenico Rossi <g.rossi@tattile.com>

 \brief
*/

#include <netdb.h>
#include <string.h>
#include <unistd.h>
#include "http.h"
#include "trace.h"


int http_open(struct http* http)
{
    struct tcp_connection *conn = &http->connection;
    struct sockaddr_in *addr = &conn->addr;
    struct timeval tv;
    
    
    if ( (conn->host = gethostbyname(http->hostname)) == NULL ) {
        perror(http->hostname);
        return -HTTP_CONNECTION_ERR;
    }
    
    conn->sd = socket(PF_INET, SOCK_STREAM, 0);
    
    bzero(addr, sizeof(*addr));
    addr->sin_family = AF_INET;
    addr->sin_port = htons(http->port);
    addr->sin_addr.s_addr = *(long*)(conn->host->h_addr);
    
    if ( connect(conn->sd, (struct sockaddr*)addr, sizeof(*addr)) != 0 ) {
        close(conn->sd);
        perror(http->hostname);
        return -HTTP_CONNECTION_ERR;
    }

    tv.tv_sec = http->timeout/1000;
    tv.tv_usec = http->timeout%1000 * 1000;
    log_debug("SND/RCV Timeout: %d s ; %d us\n", tv.tv_sec, tv.tv_usec);
    setsockopt(conn->sd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
    setsockopt(conn->sd, SOL_SOCKET, SO_SNDTIMEO, (const char*)&tv, sizeof tv);

    return HTTP_OK;
}



static
void get_error(SSL *ssl, int write_data)
{
    int err;
    
    err = SSL_get_error(ssl, write_data);
    switch (err){
        case SSL_ERROR_ZERO_RETURN:
            log_error("SSL Error (%d): ZERO_return\n", err);
            break;
        case SSL_ERROR_NONE:
            log_error("SSl Error (%d): ERROR_none \n", err);
            break;
        case SSL_ERROR_WANT_WRITE:
            log_error("SSL Error (%d): WANT_write\n", err);
            break;
        case SSL_ERROR_WANT_READ:
            log_error("SSL Errror (%d): WANT_read\n", err);
            break;            
        default:
            log_error("SSL General Error (%d)\n", err);
    }
}


int https_write(struct https *post, int size)
{
    struct http *http=(struct http*)&post->http;
    char* buf = http->data;
    int write_data = 0;
    
    write_data = SSL_write(post->session.ssl, buf, size);
    if (write_data <= 0) {
        get_error(post->session.ssl, write_data);
    }
    
    return write_data;
}

int https_read(struct https *post, int chunk)
{
    struct http *http=(struct http*)&post->http;
    char* buf = http->data;
    int read_data = 0;
    int err = 0 ;
    
    read_data = SSL_read(post->session.ssl, buf, chunk);
    if (read_data <= 0) {
        get_error(post->session.ssl, read_data);
    }    
    
    return read_data;
}

int get_content_length(char* header)
{
    char* content_length;
    int length_data = 0;
    
    content_length = strstr(header, "Content-Length: ");
    content_length += sizeof("Content-Length: ") - 1 ;
    sscanf(content_length, "%d", &length_data);
    log_debug("content-len: %d\n", length_data );

    return length_data;
}

int get_header_length(char* header)
{
    char* header_length;
    
    header_length = strstr(header, "\r\n\r\n") + sizeof("\r\n\r\n") - 1;
    log_debug("header len: %d\n", header_length - header);
    return header_length - header;
}

#if 0
int https_read(struct https *post)
{
    int size;
    struct http *http=(struct http*)&post->http;
    char* buf = http->data;
    int chunk=512;
    int err = 0; 
    int read_data = 1;
    int total_data = 0;
    int content_length;
    int header_length;
    
    
    read_data = https_read_header(post, chunk);
    content_length = get_content_length(buf);
    header_length  = get_header_length(buf);
    buf += read_data;
    total_data = read_data - header_length;
    
    /* The body data not read yet are all data (Content-Length)
       less the body data in the first read chunk  */
    content_length -= total_data;
    printf("total_data: %d content-length: %d\n", total_data, content_length);
    
    while( total_data < content_length && read_data >0){
        read_data = SSL_read(post->session.ssl, buf, chunk);
        buf += read_data;
        total_data += read_data;
    }
    printf(">>read_data: %d\n", total_data);
    
   
    return size;
}
#endif

int https_open(struct https* https)
{
    int ret;
    struct http *http = &https->http;
    struct ssl_session *s= &https->session;
    
    ret = ssl_new_instance(s);
    if(ret)
        return ret;

    ret = http_open(http);
    if(ret)
        return -HTTP_CONNECTION_ERR;    

    ret = ssl_attach_connection(s, &http->connection);
    if(ret)
        return ret;
    
    return HTTP_OK;
}



void http_close(struct http* http)
{
    close(http->connection.sd);
}

void https_close(struct https *https)
{
    struct http *http = &https->http;
    http_close(http);
    SSL_free(https->session.ssl);
    SSL_CTX_free(https->session.ctx); 
}


int https_receive(struct https* https)
{
    
    
}
