/** 
 \file wait.h
 \author Giandomenico Rossi <g.rossi@tattile.com>
 \copyright 2021 Tattile s.r.l.
 

 \brief
*/
#ifndef _WAIT_H
#define _WAIT_H


typedef int (*ftime)(void *data, va_list l);

enum WAIT_STATE {
    WAIT_OK = 0,
    WAIT_TIMEOUT=50
};


int wait_until(ftime ft, int time, void*, ...);


#endif
