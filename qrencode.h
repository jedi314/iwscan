/** 
 \file qrencode.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>
 \copyright 2022 APR
 

 \brief
*/

#ifndef _QRENCODE_H
#define _QRENCODE_H

#include <qrencode.h>

enum qr_error {
    QR_OK,
    QR_CREATE_ERR=40,
    QR_LEVEL_ERROR,
};




void dump_modules(QRcode* qrc);
void copy_modules(QRcode* qrc, char* addr);
void free_qr(QRcode* qrc);
QRcode* encode_qr(char* str, QRecLevel level, int version);
int accepted_qr_level(int level);




#endif
