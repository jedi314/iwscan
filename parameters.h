/** 
 \file parameters.h
 \author Giandomenico Rossi <g.rossi@tattile.com>

 \brief
*/


#ifndef _PARAMETERS_H
#define _PARAMETERS_H

#include <stdint.h>

struct parameter {
    uint32_t size;
    uint8_t  value[500];
};


struct parameter* increment_param(struct parameter* pin);
int write_int_param(struct parameter* pin, int32_t v);
int write_uint_param(struct parameter* pin, uint32_t v);

int write_pad_array(struct parameter* pin, uint8_t *p, int n, int pad);
int write_array_param(struct parameter* pin, uint8_t *p, int n);
int write_str_param(struct parameter* pin, char* str);
int get_str_param(struct parameter* p, char* str);
uint32_t get_uint_param(struct parameter* p);
uint32_t get_uint_param_number(struct parameter* p, int offset);


#endif


