/** 
 \file parameters.c
 \author Giandomenico Rossi <g.rossi@tattile.com>
 

 \brief
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "parameters.h"

#define PAD(a) ((~((a))+1) & 3)


struct parameter* increment_param(struct parameter* pin)
{
    int size = pin->size + PAD(pin->size) ;
    return  (struct parameter*)((uint8_t*)pin + sizeof(pin->size) + size);
}

int write_uint_param(struct parameter* pin, uint32_t v)
{
    pin->size = sizeof(v);
    *((uint32_t*)pin->value) = v;
    return pin->size;
}


int write_int_param(struct parameter* pin, int32_t v)
{
    pin->size = sizeof(v);
    *((int32_t*)pin->value) = v;
    return pin->size;
}


/**
\brief Writes a 32-bit aligned number
of bytes in memory.
The 

\param pin:
\param p:
\param l:
\param pad:
\return 
*/
int write_pad_array(struct parameter* pin, uint8_t *p, int n, int pad)
{
    pin->size = n;
    memcpy(pin->value, p, pin->size);
    memset(pin->value+n, 0, pad);
    return pin->size+pad;
}


int write_array_param(struct parameter* pin, uint8_t *p, int n)
{
    return write_pad_array(pin, p, n, 0);
}


int write_str_param(struct parameter* pin, char* str)
{
    int len; 

    len = strlen(str);
    return write_pad_array(pin, str, len, PAD(len)); 
}



int get_str_param(struct parameter* p, char* str)
{
    uint32_t size = p->size;
    snprintf(str, p->size+1, "%s", p->value);
}


uint32_t get_uint_param(struct parameter* p)
{
    return *((uint32_t*)(p->value));
}

uint32_t get_uint_param_number(struct parameter* p, int offset)
{
    int i;
    char *pc = (char*)p;
    int size;
    
    for(i=0; i<offset; i++){
        size = ((struct parameter*)pc)->size;
        size += PAD(size) + 4;
        pc += size ;
    }

    return get_uint_param((struct parameter*)pc);
}

