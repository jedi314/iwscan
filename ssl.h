/** 
 \file ssl.h
 \author Giandomenico Rossi <g.rossi@tattile.com>

 \brief
*/

#ifndef _SSL_H
#define _SSL_H

#include <openssl/ssl.h>
#include <openssl/err.h>


struct tcp_connection {
    struct hostent *host;
    struct sockaddr_in addr;    
    int sd;
};

struct ssl_session {
    SSL_CTX *ctx;
    SSL *ssl;
    int server;
    const SSL_METHOD *method;
};



enum ssl_errors {
    SSL_OK,
    SSL_NEW_CONTEXT_ERR=20,
    SSL_ATTCH_CONN_ERR,
    SSL_WRITE_ERR,
};


int ssl_init(void);
int ssl_new_instance(struct ssl_session *sess);
int ssl_attach_connection( struct ssl_session *s, struct tcp_connection *conn);


#endif

