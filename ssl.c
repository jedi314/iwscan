#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <malloc.h>
#include <string.h>
#include <sys/socket.h>
#include <resolv.h>
#include <netdb.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#include "ssl.h"
#include "http.h"


#define FAIL    -1


void show_certs(SSL* ssl)
{
    X509 *cert;
    char *line;

    cert = SSL_get_peer_certificate(ssl); /* get the server's certificate */
    if ( cert != NULL )  {
        printf("Server certificates:\n");
        line = X509_NAME_oneline(X509_get_subject_name(cert), 0, 0);
        printf("Subject: %s\n", line);
        free(line);       /* free the malloc'ed string */
        line = X509_NAME_oneline(X509_get_issuer_name(cert), 0, 0);
        printf("Issuer: %s\n", line);
        free(line);       /* free the malloc'ed string */
        X509_free(cert);     /* free the malloc'ed certificate copy */
    } else {
        printf("Info: No client certificates configured.\n");
    }
    
}


int ssl_init(void)
{
    SSL_library_init();
    
    /* Load cryptos, et.al. */    
    OpenSSL_add_all_algorithms();

    /* Bring in and register error messages */    
    SSL_load_error_strings();
        
    return SSL_OK;
}

int ssl_new_instance(struct ssl_session *sess)
{
    const SSL_METHOD *method;
    SSL_CTX *ctx;
    
    /* Create new client-method instance */    
    sess->method = TLS_client_method();
    
    /* Create new context */
    ctx = SSL_CTX_new(sess->method);
    if ( ctx == NULL ) {
        ERR_print_errors_fp(stderr);
        return -SSL_NEW_CONTEXT_ERR;
    }
    sess->ctx = ctx;
    /* create new SSL connection state */    
    sess->ssl = SSL_new(sess->ctx);     

    return SSL_OK;
}


int ssl_attach_connection( struct ssl_session *s, struct tcp_connection *conn)
{
    /* attach the socket descriptor */    
    SSL_set_fd(s->ssl, conn->sd);    
    
    if ( SSL_connect(s->ssl) == FAIL ) {   /* perform the connection */
        ERR_print_errors_fp(stderr);
        return -SSL_ATTCH_CONN_ERR;
    }
    
    return SSL_OK;
}




