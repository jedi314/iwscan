/** 
 \file ioblock.c
 \author Giandomenico Rossi <g.rossi@tattile.com>
 \copyright 2021 Tattile s.r.l.
 

 \brief Main stuff  for ram0 memory block.
 Ramo0 here is seen like a  block driver
 rather than a  mmap meory
 
*/

#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <linux/fs.h>
#include <errno.h>
#include <sys/mman.h>


#include "ioblock.h"


/**
\brief open a raw block device
and return file descriptor

\param filename: char pointer to name i.e. /dev/ram0
\return the file descriptor
*/
int open_ramdisk(struct ramdisk *rmd)
{
    int fd;
    int n, bs, ret;
    char *p;
    
    fd = open(rmd->name,  O_SYNC | O_DIRECT | O_RDWR);
    if(fd < 0) {
        perror("Open ramdisk: ");
        return -1;
    }
    rmd->fd = fd;

    ret = ioctl(fd, BLKSSZGET, &bs);
    if (ret) {
        perror("Get block size: ");
        goto exit_bs;
    }
    rmd->block.size = bs;
    ret = posix_memalign((void **)&p, bs, 1024);    
    if(ret) {
        perror("posix_memalign: ");
        goto exit_bs;
    }
    rmd->block.buffer = p;
    
    return 0;
    
exit_bs:
    close(fd);

    return ret;
}




int read_rmd(struct ramdisk *rmd, int block)
{
    off_t offset = block * rmd->block.size;
    struct ramdisk_blk *blk = &rmd->block;
    int ret;
    
    ret = moveto(rmd->fd, offset);
    if(ret < 0 )
        return ret;
    blk->id = block;
    ret = read(rmd->fd, blk->buffer, blk->size);
    if(ret < 0) {
        perror("read: ");
        return -1;
    }

    return 0;
}



/**
\brief Move over raw clock device at
offset offset

\param fd: file descriptor handler
\param offset: offset at begin of file
\return the offset value in bytes or -1
*/
off_t moveto (int fd, off_t off)
{
    off_t offset;

    offset = lseek(fd, off, SEEK_SET);
    if( offset == (off_t)-1 ) {
        perror("move to:");
        return -1;
    }
    return offset;
}



void* rmd_mmap(struct ramdisk* rmd, uint32_t blkid, int size)
{
    int ret;
    uint8_t* buf;
    int fd = rmd->fd;
    
    buf = (uint8_t*)mmap(NULL, size, PROT_READ|PROT_WRITE, MAP_SHARED, fd, blkid);
    if(buf == MAP_FAILED) {
        perror("rmd_mmap: ");
    }
    
    return buf;
}

