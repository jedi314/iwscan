/** 
 \file aplist.c
 \author Giandomenico Rossi <g.rossi@tattile.com>
 

 \brief Access point list stuff
*/

#include <pthread.h>
#include <time.h>
#include "trace.h"
#include "parameters.h"
#include "aplist.h"
#include "netinfo.h"
#include "http.h"
#include "wait.h"
#include "log/log.h"
#include "qrencode.h"

static
struct parameter* fill_ap(struct ap_info *ap, struct parameter *pout)
{
    write_str_param(pout, ap->ssid);
    pout = increment_param(pout);
    write_array_param(pout, ap->mac, 6);
    pout = increment_param(pout);
    write_uint_param(pout, ap->quality);
    pout = increment_param(pout);
    write_uint_param(pout, ap->enc);
    pout = increment_param(pout);

    return pout;
}

static
int pack_ap(struct command_out* cmdout, struct ap_info *api, int n)
{
    struct parameter *pout = (struct parameter*)cmdout->params;
    struct ap_info *ap;
    int i, np=0, res;

    for(i=0; i<n; i++) {
        ap = &api[i];
        
        if(ap->ssid[0] == 0)
            continue;

        if(strncmp(ap->ssid, "\\x00", 4) == 0 ){
            ap->ssid[0]=0;
        }
        
        pout = fill_ap(ap, pout);

        if ( is_trace_level() ){
            printf("SSID: %s%*c"
                   "mac: %02x:%02x:%02x:%02x:%02x:%02x\t"
                   "fq: %uMHz\t"
                   "level: %ddBm\t"
                   "enc: %d\n",
                   api[i].ssid, 20-strlen( api[i].ssid), ' ',
                   api[i].mac[0], api[i].mac[1],
                   api[i].mac[2], api[i].mac[3],
                   api[i].mac[4], api[i].mac[5],
                   api[i].channel, api[i].level, api[i].enc );    
        }
        
        np++;
    }

    return np*4;
}


// wpa_supplicant -D nl80211 -i wlan0 -c /etc/wpa_supplicant.conf -
// udhcpc -R -n -O search -p /var/run/udhcpc.wlan0.pid -i wlan0

int get_wifi_ap(struct command_in* cmdin, struct command_out* cmdout, void* param)
{
    struct ramdisk *ramd = (struct ramdisk*)param;
    struct wifi_list* wifi = &ramd->wifi;
    //struct parameter *pin = (struct parameter*)cmdout->params;
    int i, n = 0;
    char* list;
    struct ap_info *api;
    int n_ap;
    
    
    /* lock */
    if (ramd->thread)
        pthread_mutex_lock(&wifi->lock);
    else{
        free(wifi->ap_info);
        wifi->ap_info = scan_wifi_ap(wifi, &n_ap);
        wifi->n_ap = n_ap;
    }
    api = wifi->ap_info;
    n = pack_ap(cmdout, api, wifi->n_ap);
    if (ramd->thread)
        pthread_mutex_unlock(&wifi->lock);
    
    return n;
}


static
int connection(struct wifi_status *status)
{
    return status->connection == 1;
}

int connect_to_ap(struct command_in* cmdin, struct command_out* cmdout, void* param)
{
    struct ramdisk *ramd = (struct ramdisk*)param;
    struct wifi_list* wifi = &ramd->wifi;
    struct parameter *pin =  (struct parameter*)cmdin->params;
    struct ap_info  *ap, api;
    char essid[128];
    char key[128];
    int err = 0;
    struct wifi_status status;

    
    get_str_param(pin, essid);

    if (ramd->thread)
        pthread_mutex_lock(&wifi->lock);
    
    ap = look_for_essid(wifi, essid);
    if (ramd->thread)
        pthread_mutex_unlock(&wifi->lock);

    if(ap == NULL){
        log_warn("Warning: %s not found in list\n", essid);
    }

    pin = increment_param(pin);
    get_str_param(pin, key);
    
    err = wifi_disconnect(wifi);
    if(err)
        return err ;
    
    err = wifi_connect(wifi, essid, key);
    if (err)
        return err;
    TRACE;

    usleep(2000000);
    wifi_status(wifi, &status);

    if(! connection(&status) ){
        log_error("Error Status after connection %d\n", status.connection);
        wifi_disconnect(wifi);
        return WIFI_CONNECTION_ERROR;
    }
    
    err = get_dhcp("wlan0");
    if(err)
        wifi_disconnect(wifi);

    log_info("Connection OK\n", status.connection);
    return err;
}


int remove_to_ap(struct command_in* cmdin, struct command_out* cmdout, void* param)
{
    struct ramdisk *ramd = (struct ramdisk*)param;
    struct wifi_list* wifi = &ramd->wifi;
    struct parameter *pin =  (struct parameter*)cmdin->params;
    struct ap_info*  ap;
    int err = 0;

    err = wifi_disconnect(wifi);

    return err;
}

static 
int pack_status( struct command_out* cmdout, char* buf)
{
    char ip_str[]  = "ip_address=";
    char mac_str[] = "address=";
    char *p1, *p2;
    int n;
    int n1 = sizeof(ip_str)/sizeof(ip_str[0]); 
    int n2 = sizeof(mac_str)/sizeof(mac_str[0]);
    
    p1 = strstr(buf, ip_str) + n1;
    p2 = strstr(p1, mac_str) + n2;
    p1[p2-p1-n2-1] = 0;
    
}


int pack_network_info(struct command_out* cmdout, struct net_info* ni)
{
    struct parameter *pout = (struct parameter*)cmdout->params;
    struct ap_info *ap;
    int i, n=0, res;
    uint8_t nulldns[4] = {0,0,0,0};
    uint8_t *dns;
    
    write_array_param(pout, ni->mac, 6);
    pout = increment_param(pout);
    n++;

    write_array_param(pout, ni->addr, 4);
    pout = increment_param(pout);
    n++;

    write_array_param(pout, ni->mask, 4);
    pout = increment_param(pout);
    n++;

    write_array_param(pout, ni->gateway, 4);
    pout = increment_param(pout);
    n++;

    dns = ni->dns[0] ? ni->dns[0] : nulldns; 
    write_array_param(pout, dns , 4);
    pout = increment_param(pout);
    n++;

    dns = ni->dns[1] ? ni->dns[1] : nulldns; 
    write_array_param(pout, dns, 4);
    pout = increment_param(pout);
    n++;
    
    return n;
}


int  get_network_info(struct command_in* cmdin, struct command_out* cmdout, void* param)
{
    struct ramdisk *ramd = (struct ramdisk*)param;
    struct wifi_list* wifi = &ramd->wifi;
    struct parameter *pin =  (struct parameter*)cmdin->params;
    int err = 0;
    int n = 0;
    char iface[] = "wlan0";
    struct net_info  ni;

    ni.dns[0] = 0;
    ni.dns[1] = 0;    

    if (wifi->active_ap == -1){
        log_error("No access point connected\n");
        return -DISABLE_NETWORK_ERR;
    }
    
    get_netinfo(iface, &ni);
    get_gateway(&ni);
    get_dns_servers(&ni);

    log_info(
        "Mac: %x:%x:%x:%x:%x:%x\n"
        "Address: %02d.%02d.%02d.%02d\n"
        "Netmask: %02d.%02d.%02d.%02d\n"
        "Gateway: %02d.%02d.%02d.%02d\n"
        "DNS: %02d.%02d.%02d.%02d\n", 
        ni.mac[0], ni.mac[1], ni.mac[2], ni.mac[3], ni.mac[4], ni.mac[5],
        ni.addr[0], ni.addr[1], ni.addr[2], ni.addr[3], 
        ni.mask[0], ni.mask[1], ni.mask[2], ni.mask[3], 
        ni.gateway[0], ni.gateway[1], ni.gateway[2], ni.gateway[3],
        ni.dns[0][0], ni.dns[0][1], ni.dns[0][2], ni.dns[0][3]);
    
    n = pack_network_info(cmdout, &ni); 
    free(ni.dns[0]);
    free(ni.dns[1]);
    
    return n;    
}


int connection_status(struct command_in* cmdin, struct command_out* cmdout, void* param)
{
    struct ramdisk *ramd = (struct ramdisk*)param;
    struct parameter *pout = (struct parameter*)cmdout->params;
    struct wifi_list* wifi = &ramd->wifi;
    struct ap_info* ap;
    struct wifi_status status;
    int n=0;
    
    wifi_status(wifi, &status);
    
    write_int_param(pout, -status.connection);
    log_info("Connection: %d\n", -status.connection);
    n++;
    if(status.connection) {
        pout = increment_param(pout);
        write_int_param(pout, status.quality);
        log_info("Quality: %d\n", status.quality);
        pout = increment_param(pout);
        n++;
        write_uint_param(pout, status.channel);
        log_info("Channel: %d\n", status.channel);
        n++;
    }
    return n;
}

int get_release(struct command_in* cmdin, struct command_out* cmdout, void* param)
{
    struct ramdisk *ramd = (struct ramdisk*)param;
    struct parameter *pout = (struct parameter*)cmdout->params;
    struct wifi_list* wifi = &ramd->wifi;
    struct ap_info* ap;
    struct wifi_status status;
    int n=0;
    
    write_int_param(pout, ramd->release.version);
    n++;

    return n;
}



static
uint32_t cmdblock_inoffset(uint32_t block)
{
    return (block-IN_DATA_BLOCK) * BLK_SIZE;
}

static
uint32_t cmdblock_outoffset(uint32_t block)
{
    return (block-OUT_DATA_BLOCK) * BLK_SIZE;
}



static
uint8_t* cmdblock_addr(struct ramdisk *ramd, uint32_t block)
{
    return ramd->indata + cmdblock_inoffset(block);
}


static
uint8_t* outblock_addr(struct ramdisk *ramd, uint32_t block)
{
    return ramd->outdata + cmdblock_outoffset(block);
}

/* static */
/* time_t wait_time(time_t timeout) */
/* { */
/*     return time(NULL)+timeout; */
/* } */

static
int data_ready(void* par, va_list ap )
{
    uint32_t bytes;
    struct parameter *pin=(struct parameter*)par;
    uint32_t* avail_bytes; 


    avail_bytes = va_arg(ap, uint32_t* );
    log_debug("Avail: %d\n", *avail_bytes);

    if((bytes = get_uint_param(pin)) > *avail_bytes){
        *avail_bytes = bytes;
        return HTTP_OK;
    }
    log_debug("bytes: %d avail: %d\n", bytes, *avail_bytes);

    return WAIT_OK;
}

/* static */
/* int wait_until_data_ready(struct parameter *pin, uint32_t *avail_bytes) */
/* { */
/*     int res ; */

/*     res = wait_until(data_ready, 2, pin, avail_bytes); */
/*     return res == WAIT_OK ? HTTP_OK : -HTTP_WAIT_TIMEOUT; */
/* } */



static
time_t wait_time(time_t timeout)
{
    return time(NULL)+timeout;
}

static
int wait_until_data_ready(struct parameter *pin, uint32_t *avail_bytes)
{
    uint32_t bytes;
    int wait_timeout = wait_time(HTTP_WAIT_TIMEOUT);

    
    while ( time(NULL) < wait_timeout) {
        if((bytes = get_uint_param(pin)) > *avail_bytes){
            *avail_bytes = bytes;
            return HTTP_OK;
        }
    }
    return -HTTP_WAIT_TIMEOUT;
}




int command_aborted(struct command_in* cmdin)
{
    struct command_in* cmda;
    struct parameter *pin; 
    uint32_t key;
        
    cmda = (struct command_in*)((char*)cmdin + BLK_SIZE);
    pin = (struct parameter*)cmda->params;    
    
    if (cmdin->seq != cmda->seq)
        return 0;
    
    if (cmdin->id != cmda->id)
        return 0;

    key = get_uint_param(pin);
    
    if ( key == ABORT_KEY )
        return 1;

    return 0;
}




static
int https_transfer(struct https* https, struct command_in *cmdin, struct parameter **ppin, int wait_data)    
{
    uint32_t total_bytes = 0;
    uint32_t block;
    uint32_t avail_bytes = 0;
    uint32_t sent_bytes = 0;
    int32_t n_bytes = 0;
    struct http *http = &https->http;
    struct parameter *pin = *ppin;
    int ntry=3;

    pin = increment_param(pin);
    block = get_uint_param(pin);
    http->data += cmdblock_inoffset(block);

    pin = increment_param(pin);
    total_bytes = get_uint_param(pin);
    
    if (wait_data)
        pin = increment_param(pin);
    else
        avail_bytes = total_bytes;
    
    log_debug("%d hostname: %s- Sent Bytes: %d Total Bytes: %d - Block#: %d - Avail. bytes: %d\n",
           wait_data,  http->hostname, sent_bytes, total_bytes, block, avail_bytes);

    while(sent_bytes < total_bytes){
        if(wait_data){
            if(wait_until_data_ready(pin, &avail_bytes) == -HTTP_WAIT_TIMEOUT ){
                log_warn("Wait 1s without new data\n");
                ntry++;
                if(ntry==3)
                    return -HTTP_UPDATE_TIMEOUT;
                continue;
            }
        }
        n_bytes = https_write(https, avail_bytes - sent_bytes);
        log_debug("Sent %d bytes\n", n_bytes );
        if(n_bytes <= 0){
            return -SSL_WRITE_ERR;
        }
        if(command_aborted(cmdin)){
            return -ABORTED;
        }
        
        http->data += n_bytes;
        sent_bytes += n_bytes;
    }
    log_debug("exit from transfer total %d bytes\n", sent_bytes );
    
    *ppin = pin;
    return HTTP_OK;
}

static
int update_written_blocks( struct parameter *pout, int* n_blocks, int total_data, int tb)
{
    int written_blocks;
    struct parameter *data_ready;
    
    data_ready = ( struct parameter *)((char*)pout + 16);
    written_blocks = (total_data + BLK_SIZE - 1) / BLK_SIZE;
    if ( (written_blocks - *n_blocks)  / UPDATE_N_BLOCKS > 0){
        *n_blocks = written_blocks;
        write_int_param(data_ready, total_data);
       log_debug("body_read: %09d - blocks: %02d [%02d%%]               \r",
               total_data, written_blocks, written_blocks*100/tb);
    }
    
    return written_blocks;
}
    
static
int https_receive(struct https* https, struct command_in* cmdin,  struct parameter *pout)
{
    uint32_t recv_bytes = 0;
    struct http *http = &https->http;
    int n = 0;
    int chunk=512;
    int read_data = 0;
    int content_length;
    int header_length;
    int total_data=0;
    int n_blocks=0;
    int total_blocks;
    int total_written_blocks=1;
    
    
    read_data = https_read(https, chunk);
    if (read_data < 0)
        return -HTTPS_READ_ERROR;
    /* The body data not read yet are all data (Content-Length)
       less the body data in the first read chunk  */
    content_length = get_content_length(http->data);
    header_length  = get_header_length(http->data);
    http->data += read_data;
    total_data = read_data - header_length;
    total_blocks = ( content_length+header_length + BLK_SIZE - 1 )/BLK_SIZE;
    while( total_data < content_length){
        read_data = https_read(https, chunk);
        if (read_data < 0)
            return -HTTPS_READ_ERROR;
        http->data += read_data;
        total_data += read_data;
        /* update  */
        total_written_blocks = update_written_blocks(pout, &n_blocks, total_data, total_blocks);
        if(command_aborted(cmdin)){
            return -ABORTED;
        }
    }
    log_debug("Total blocks: %d - message_length: %d\n",
           total_written_blocks, header_length + content_length);
    
    write_int_param(pout, OUT_DATA_BLOCK);
    pout = increment_param(pout);
    n++;
    
    write_int_param(pout, content_length + header_length);
    pout = increment_param(pout);
    n++;
    
    /* P2 is alway update in loop  */
    write_int_param(pout, total_written_blocks);
    n++;
    
    return n;
}

static
int https_send_message(struct https* https, struct command_in* cmdin, struct parameter **pin)
{
    return https_transfer(https, cmdin, pin, 0);
}


static
int https_send_attachment(struct https* https, struct command_in* cmdin,  struct parameter **pin)
{
    return https_transfer(https, cmdin, pin, 1);
}


int https_send(struct command_in* cmdin, struct command_out* cmdout, void* param)
{
    struct ramdisk *ramd = (struct ramdisk*)param;
    struct parameter *pout = (struct parameter*)cmdout->params;
    struct parameter *pin = (struct parameter*)cmdin->params;
    struct https https;
    struct http *http = &https.http;
    int ret;
    
    http->port = 443;

                   
    get_str_param(pin, http->hostname);
    if(cmdin->n_param == 7)
        http->timeout = get_uint_param_number(pin, 6);
    else{
        http->timeout = 10000;
    }
    log_debug("OPEN %d \n", http->timeout);
    ret = https_open(&https);
    if(ret){
        return ret;
    }
    
    http->data = ramd->indata;
    log_debug("Send Messsage\n");
    ret = https_send_message(&https, cmdin, &pin);
    if(ret){
        goto close_connection;
    }
    
    http->data = ramd->indata;
    log_debug("Send attachment\n");
    ret = https_send_attachment(&https, cmdin, &pin);
    if(ret < 0 ){
        goto close_connection;
    }
    
    TRACE;
    http->data = outblock_addr(ramd, OUT_DATA_BLOCK);
    log_debug("Receive\n");
    ret = https_receive(&https, cmdin, pout);
    
close_connection:
    log_debug("Try Close\n");
    https_close(&https);
    log_debug("Close OK\n");

    return ret;
}

int opkg(char* param, char* out, int size)
{
    FILE *opkg;
    char cmd[256]={0};
    int ret = 0;
    int n;
    char* pout=out;
    
    sprintf(cmd, "/usr/bin/opkg --no-check-certificate %s", param);
    opkg = popen(cmd, "r");

    /* Read the output a line at a time - output it. */
    if(out)
        fgets(out, size, opkg);
    
    ret = pclose(opkg);
    return ret;
}


/* static */
/* int upgrade_ready() */
/* { */
/*     char status; */

/*     if(update_error()) */
/*         return 0; */

/*     if(not_installed() */
    
/* } */


struct upgrade {
    uint32_t new;
    uint32_t version;
    char* cmd;
};

static
int get_iw_release(char* cmd, struct release *rel)
{
    int ret;
    char out[256]={0};

    rel->version = 0;
    ret = opkg(cmd , out, sizeof(out));
    out[strcspn(out, "\n")] = 0;

    if(strcmp(out, "") != 0) {
        sscanf(out, "%hhu.%hhu.%hhu", &rel->major, &rel->minor, &rel->patch);
        log_debug("release: %d.%d.%d\n", rel->major, rel->minor, rel->patch);
        return 1;
    }    
    return 0;
}

static
void  check_upgradable(struct upgrade* upg)
{
    int ret;
    char out[256];
    struct release rel;
    rel.version = 0;
    
    if (get_iw_release(upg->cmd, &rel)){
        upg->new = -1;
    }else{
        upg->new = 0;
    }
    upg->version = rel.version;
}


static
int not_installed()
{
    int ret;
    char out[256];
    struct release rel;
    rel.version = 0;

    ret = opkg("list iwscan | grep Status | awk '{print $2}'" , out, sizeof(out));
    out[strcspn(out, "\n")] = 0;
    if(strcmp(out, "unknown") != 0) {
        return 1;
    }
    return 0;
}

int upgradable_release(struct command_in* cmdin, struct command_out* cmdout, void* param)
{
    struct ramdisk *ramd = (struct ramdisk*)param;
    struct parameter *pout = (struct parameter*)cmdout->params;
    struct parameter *pin = (struct parameter*)cmdin->params;
    int n=0;
    uint8_t m,nn,j;
    struct upgrade upg;
    
    /* if(not_installed()){ */
    /*     upg.cmd="list | grep iwscan | awk '{print $3}'"; */
    /*     printf("Not Installed!\n"); */
    /* }else */
        
    upg.cmd="list-upgradable | grep iwscan | awk '{print $5}'";
    check_upgradable(&upg);
    write_int_param(pout, upg.new);
    pout = increment_param(pout);
    n++;
    if( upg.new) {
        write_int_param(pout, upg.version);
        pout = increment_param(pout);
        n++;
    }
      
    return n;
}

int upgrade_release(struct command_in* cmdin, struct command_out* cmdout, void* param)
{
    struct ramdisk *ramd = (struct ramdisk*)param;
    struct parameter *pout = (struct parameter*)cmdout->params;
    struct parameter *pin = (struct parameter*)cmdin->params;
    int ret, n = 0;
    
    ret = opkg("upgrade iwscan", NULL, 0);
    
    return 0;
}


int qr_encode(struct command_in* cmdin, struct command_out* cmdout, void* param )
{
    struct ramdisk *ramd = (struct ramdisk*)param;
    struct parameter *pout = (struct parameter*)cmdout->params;
    struct parameter *pin = (struct parameter*)cmdin->params;
    QRcode* q= NULL;
    int strblock;
    int strlen, level, n=0;
    char* str;
    
    strblock = get_uint_param(pin);
    pin = increment_param(pin);
    
    strlen = get_uint_param(pin);
    pin = increment_param(pin);

    level = get_uint_param(pin);
    str = ramd->indata + cmdblock_inoffset(strblock);
    str[strlen] = 0;

    log_info("string: %s\n", str);
    
    if(!accepted_qr_level(level))
        return QR_LEVEL_ERROR;

    log_info("QR Level: %d\n", level);

    q = encode_qr(str, level, 1);
    if (q == NULL )
        return -QR_CREATE_ERR;

    dump_modules(q);
    copy_modules(q, outblock_addr(ramd, OUT_DATA_BLOCK));

    write_int_param(pout, q->width);
    pout = increment_param(pout);
    n++;


    write_int_param(pout, OUT_DATA_BLOCK);
    pout = increment_param(pout);
    n++;
    
    free_qr(q);

    return n;
}
