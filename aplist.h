/** 
 \file aplist.h
 \author Giandomenico Rossi <g.rossi@tattile.com>

 \brief
*/

#ifndef _AP_LIST_H
#define _AP_LIST_H

#include "ramdisk.h"
#include "wifi.h"

int get_wifi_ap(struct command_in* cmdin, struct command_out* cmdout, void* param);
int connect_to_ap(struct command_in* cmdin, struct command_out* cmdout, void* param);
int remove_to_ap(struct command_in* cmdin, struct command_out* cmdout, void* param);
int get_network_info(struct command_in* cmdin, struct command_out* cmdout, void* param);
int connection_status(struct command_in* cmdin, struct command_out* cmdout, void* param);

int get_release(struct command_in* cmdin, struct command_out* cmdout, void* param);
int https_send(struct command_in* cmdin, struct command_out* cmdout, void* param);

int upgradable_release(struct command_in* cmdin, struct command_out* cmdout, void* param);
int upgrade_release(struct command_in* cmdin, struct command_out* cmdout, void* param);
int qr_encode(struct command_in* cmdin, struct command_out* cmdout, void* param );


#endif 
