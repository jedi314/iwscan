/** 
 \file netinfo.h
 \author Giandomenico Rossi <g.rossi@tattile.com>

 \brief
*/

#ifndef _NETINFO_H
#define _NETINFO_H


struct net_info {
    uint8_t addr[4];
    uint8_t mask[4];
    uint8_t gateway[4];
    uint8_t mac[6];
    uint8_t* dns[2];
};


int get_netinfo(char* iface, struct net_info *ni);
int  get_gateway(struct net_info* ni);
int get_dns_servers(struct net_info* ni);


#endif
