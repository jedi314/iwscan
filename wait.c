/** 
 \file wait.c
 \author Giandomenico Rossi <g.rossi@tattile.com>
 \copyright 2021 Tattile s.r.l.
 

 \brief
*/

#include <time.h>
#include <stdarg.h>


#include <stdio.h>
#include "wait.h"

static
time_t wait_time(time_t timeout)
{
    return time(NULL)+timeout;
}

/**
\brief General wait function
Get a callback function as fist 
partameter used as exit-true condition
or a timeout

\param ft: calback function to test truth
\param t: timeout
\param: extra params

\return 0 or error WAIT_TIMEOUT
*/
int wait_until(ftime ft, int t, void* data, ...)
{
    va_list ap;
    int wait_timeout = wait_time(t);
    int ret=WAIT_OK;
    
    va_start (ap, data);
    
    while ( time(NULL) < wait_timeout) {
        if ( ft(data, ap) == WAIT_OK ){
            printf("Exit\n");
            ret = WAIT_OK;
            goto exit;
        }
    }
    ret = -WAIT_TIMEOUT;

exit:
    va_end(ap);
    return ret; 
}


