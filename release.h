/** 
 \file release.h
 \author Giandomenico Rossi <gdrossi@hotmail.com>

 \brief
*/

#ifndef _RELEASE_H
#define _RELEASE_H

#define RELEASE_MAJOR_NUMBER 1
#define RELEASE_MINOR_NUMBER 1
#define RELEASE_PATCH_NUMBER 3

#endif



